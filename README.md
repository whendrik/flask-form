# Example of Simple Flask example, handling a form

Change the `app.py` in the `app` directoy to do something usefull with the form values, e.g. call some REST API.

Currently the form values are send back to the web-app for testing purposes.

## Build Example with docker

```
docker build . -t simpleform
```

## Run Example with docker

```
docker run --rm -p 8080:8080 simpleform
```

## Run on Open-Shift

Create the app, optionally use `--name=flask-app`
```
oc new-app https://gitlab.com/whendrik/flask-form.git
```

Delete the app with
```
oc delete all --selector app=flask-app
```

Log the build process
```
oc logs -f bc/simpleform
```

Check which `CLUSTER_IP` is our server
```
oc get svc
```

## Test the app

On the cluster do
```
curl CLUSTER_IP:8080
```

Or with ssh port forwarding with `ssh -L 127.0.0.1:8080:CLUSTER_IP:8080 OPENSHIFT_MASTER`

---

Optional - expose the service by creating a route

```
oc expose svc/simpleform
```