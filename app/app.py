import os
import requests

from flask import Flask, render_template, request, jsonify


PORT = int(os.environ.get('OPENSHIFT_PYTHON_PORT', 8080))

app = Flask(__name__, static_url_path='/static')


@app.route('/')
def Welcome():
	return render_template('index.htm')

@app.route('/score', methods=['GET', 'POST'])
def process_form_data():
	# Get the form data - result will contian all elements from the HTML form that was just submitted
	result = request.form

	print(result)
	
	print(result['ISIN'])
	print(result['start_date'])
	print(result['end_date'])

	# requests.post(...)


	json_response = request.form

	return jsonify( json_response ) 

if __name__ == '__main__':
	app.run(host='0.0.0.0', port=PORT)